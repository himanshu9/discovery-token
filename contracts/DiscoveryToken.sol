pragma solidity 0.4.24;

import "./Pausable.sol";
import "./Owned.sol";
import "./ERC20.sol";
import "./SafeMath.sol";
import "./StandardToken.sol";

 /**
 * @title Discovery token
 */
contract DiscoveryToken is StandardToken, Owned, Pausable {
    using SafeMath for uint;
    string public symbol;
    string public name;
    uint8 public decimals;
    uint public totalReleased;
    uint public tokensForSale = 510000000 * 1 ether;
    uint public teamTokens = 120000000 * 1 ether;
    uint public advisorsTokens = 70000000 * 1 ether;
    uint public marketingTokens = 25000000 * 1 ether;
    uint public bountyTokens = 25000000 * 1 ether;
    uint public partnersTokens = 100000000 * 1 ether;
    uint public treasuryTokens = 150000000 * 1 ether;
    uint public icoStartBlock;
    address public saleContract;
    address public vestingContract;
    bool public fundraising = true;
 
    mapping (address => bool) public frozenAccounts;
    event FrozenFund(address target, bool frozen);
    event PriceLog(string text);
    event ReleasedPartnerTokens(address partner, uint256 tokens);
    event ReleasedTreasuryTokens(address partner, uint256 tokens);

    modifier onlyPayloadSize(uint numWords) {
        assert(msg.data.length >= numWords * 32 + 4);
        _;
    }

    modifier manageTransfer() {
        if (msg.sender == owner) {
            _;
        } else {
            require(fundraising == false);
            _;
        }
    }


   /// @dev Initialize the token contract 
   /// @param _tokenOwner owner of the token contract 
    constructor (address _tokenOwner ) public Owned(_tokenOwner) {
        symbol ="XIOT";
        name = "XIOT";
        decimals = 18;
        totalSupply = 1000000000 * 1 ether;
    }
  
    function transfer(address _to, uint256 _value) public manageTransfer whenNotPaused onlyPayloadSize(2) returns (bool success) {
        require(_value>0);
        require(_to != address(0));
        require(!frozenAccounts[msg.sender]);
        super.transfer(_to,_value);
        return true;
    }

    function transferFrom(address _from, address _to, uint256 _value) public manageTransfer whenNotPaused onlyPayloadSize(3) returns (bool) {
        require(_to != address(0));
        require(_from != address(0));
        require(!frozenAccounts[msg.sender]);
        super.transferFrom(_from,_to,_value);
        return true;
    }

    /// @dev activates the sale and vesting contract (i.e. transfers saleable and vestable tokens to respective contracts)
    /// @param _saleContract ,address of crowdsale contract

    function activateSaleContract(address _saleContract) public whenNotPaused onlyOwner {
        require(_saleContract != address(0));
        require(saleContract == address(0));
        saleContract = _saleContract;
        balances[saleContract] = balances[saleContract].add(tokensForSale);
        totalReleased = totalReleased.add(tokensForSale);
        tokensForSale = 0;  
        icoStartBlock = block.number;
        assert(totalReleased <= totalSupply);
        emit Transfer(address(this), saleContract, 510000000 * 1 ether);
    }

     function activateVestingContract(address _vestingContract) public whenNotPaused onlyOwner {
        
        require(_vestingContract != address(0));
        require(vestingContract == address(0));
        vestingContract = _vestingContract;
        uint256 vestableTokens = teamTokens.add(advisorsTokens).add(marketingTokens);
        balances[vestingContract] = balances[vestingContract].add(vestableTokens);
        totalReleased = totalReleased.add(vestableTokens);
        assert(totalReleased <= totalSupply);
    }

    /**
    * @dev function to check whether passed address is a contract address
    */
    function isContract(address _address) private view returns (bool is_contract) {
        uint256 length;
        assembly {
        //retrieve the size of the code on target address, this needs assembly
            length := extcodesize(_address)
        }
        return (length > 0);
    }
           
    /// @dev Burns a specific amount of tokens.
    /// @param _value The amount of token to be burned.
    function burn(uint _value) public whenNotPaused returns (bool success) {
        require(_value>0);
        require(balances[msg.sender] >= _value);
        balances[msg.sender] = balances[msg.sender].sub(_value);
        totalSupply = totalSupply.sub(_value);
        totalReleased = totalReleased.sub(_value);
        emit Burn(msg.sender, _value);
        return true;
    }
  


    /**
   * @dev this function can only be called by crowdsale contract to transfer tokens to investor
   * @param _to address The address of the investor.
   * @param _value uint256 The amount of tokens to be send
   */
    function saleTransfer(address _to, uint256 _value) public whenNotPaused returns (bool) {
        require(saleContract != address(0));
        require(msg.sender == saleContract);
        return super.transfer(_to, _value);
    }

     /**
   * @dev this function can only be called by owner to transfer partner tokens to any partner address
   * @param _partner address The address of the partner.
   * @param _value uint256 The amount of tokens to be send
   */
    function releasePartnersTokens(address _partner, uint256 _value) public whenNotPaused onlyOwner {
       require(_value > 0);
       require(partnersTokens > 0);
       balances[_partner] = balances[_partner].add(_value * 1 ether);
       partnersTokens =  partnersTokens.sub(_value * 1 ether);
       totalReleased = totalReleased.add(_value * 1 ether);
       emit ReleasedPartnerTokens(_partner,_value);

    }
   /**
   * @dev this function can only be called by owner to transfer treasury tokens to any  address
   * @param _to address The address of the token receiver.
   * @param _value uint256 The amount of tokens to be send
   */
    function releaseTreasuryTokens(address _to, uint256 _value) public whenNotPaused onlyOwner {
       require(_value > 0);
       require(treasuryTokens > 0);
       balances[_to] = balances[_to].add(_value * 1 ether);
       treasuryTokens =  treasuryTokens.sub(_value * 1 ether);
       totalReleased = totalReleased.add(_value * 1 ether);
       emit ReleasedTreasuryTokens(_to,_value);
    }

   /**
   * @dev this function can only be called by  contract to transfer tokens to vesting beneficiary
   * @param _to address The address of the beneficiary.
   * @param _value uint256 The amount of tokens to be send
   */
    function vestingTransfer(address _to, uint256 _value) public whenNotPaused returns (bool) {
        require(vestingContract != address(0));
        require(msg.sender == vestingContract);
        return super.transfer(_to, _value);
    }

    /**
   * @dev this function will burn the unsold tokens after crowdsale is over and this can be called
   *  from crowdsale contract only when crowdsale is over
   */
    function burnTokensForSale() public whenNotPaused returns (bool) {
        require(saleContract != address(0));
        require(msg.sender == saleContract);
        uint256 tokens = balances[saleContract];
        require(tokens > 0);
        require(tokens <= totalSupply);
        balances[saleContract] = 0;
        totalSupply = totalSupply.sub(tokens);
        totalReleased = totalReleased.sub(tokens);
        emit Burn(saleContract, tokens);
        return true;
    }

   /**
   * @dev this function will closes the sale ,after this anyone can transfer their tokens to others.
   */
    function finalize() public whenNotPaused {
        require(fundraising != false);
        require(msg.sender == saleContract);
        // Switch to Operational state. This is the only place this can happen.
        fundraising = false;
    }


  /**
   * @dev this function will freeze the any account so that the frozen account will not able to participate in crowdsale.
   * @param target ,address of the target account 
   * @param freeze ,boolean value to freeze or unfreeze the account ,true to freeze and false to unfreeze
   */
   function freezeAccount (address target, bool freeze) public onlyOwner {
        require(target != 0x0);
        require(freeze == (true || false));
        frozenAccounts[target] = freeze;
        emit FrozenFund(target, freeze); // solhint-disable-line
    }
    /**
   * @dev this function will send the bounty tokens to given address
   * @param _to ,address of the bounty receiver.
   * @param _value , number of tokens to be sent.
   */
    function sendBounty(address _to, uint256 _value) public whenNotPaused onlyOwner returns (bool) {
        require(_value>0);
        uint256 value = _value.mul(1 ether);
        require(bountyTokens >= value);
        totalReleased = totalReleased.add(value);
        require(totalReleased <= totalSupply);
        balances[_to] = balances[_to].add(value);
        bountyTokens = bountyTokens.sub(value);
        emit Transfer(address(this), _to, value);
        return true;
   }


   /**
   * @dev Function to transfer any ERC20 token  to owner address which gets accidentally transferred to this contract
   * @param tokenAddress The address of the ERC20 contract
   * @param tokens The amount of tokens to transfer.
   * @return A boolean that indicates if the operation was successful.
   */
    function transferAnyERC20Token(address tokenAddress, uint tokens) public whenNotPaused onlyOwner returns (bool success) {
        require(tokenAddress != address(0));
        require(isContract(tokenAddress));
        return ERC20(tokenAddress).transfer(owner, tokens);
    }

 
    function () public payable {
        revert();
    }
    
}