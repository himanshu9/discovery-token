pragma solidity  0.4.24;
import "./SafeMath.sol";
import "./Owned.sol";
import "./Oraclize.sol";

contract DiscoveryToken {
    function transfer (address, uint) public;
    function burnTokensForSale() public returns (bool);
    function saleTransfer(address, uint256) public  returns (bool);
    function finalize() public;
}


contract Crowdsale is Owned, usingOraclize { 
  using SafeMath for uint256;
  //oraclize related state variables
  uint256 public ethPrice; // 1 Ether price in USD cents.
  uint256 constant CUSTOM_GASLIMIT = 150000;
  uint256 public updateTime = 0;
  // end oraclize variables

  //Oraclize events
    event LogConstructorInitiated(string nextStep);
    event newOraclizeQuery(string description);
    event newPriceTicker(bytes32 myid, string price, bytes proof);
  // End oraclize events

  // The token being sold
  DiscoveryToken public token;


  // Address where funds are collected
  address public wallet;

  // Amount of wei raised
  bool public crowdsaleStarted = false; 
  uint256 public privateSaleCap = 110000000; //  cap in cents 1.1 m
  uint256 public presaleRoundOneCap = 200000000; //  cap in cents 2 m 
  uint256 public presaleRoundTwoCap = 500000000; //  cap in cents 5 m
  uint256 public publicsaleRoundOneCap = 600000000; // 6 m cap in cents 
  uint256 public publicsaleRoundTwoCap = 680000000; // 6.8 m cap in cents
  uint256 public totalCap = 2090000000; //20.9 million dollar cap in cents;
  
  uint256 public privateSaleTokens = 44000000; 
  uint256 public preSaleRoundOneTokens = 56000000;  
  uint256 public preSaleRoundTwoTokens = 130000000; 
  uint256 public publicSaleRoundOneTokens = 144000000;  
  uint256 public publicSaleRoundTwoTokens = 136000000; 
  
  uint256 public privateSaleRaised = 0;
  uint256 public presaleRoundOneRaised = 0;
  uint256 public presaleRoundTwoRaised = 0;  
  uint256 public publicsaleRoundOneRaised = 0; 
  uint256 public publicsaleRoundTwoRaised = 0; 
  uint256 public totalRaisedInCents;
  
  uint256 public privateSaleTokensPerDollar = 40 * 1 ether;
  uint256 public presaleRoundOneTokensPerDollar = 28 * 1 ether;
  uint256 public presaleRoundTwoTokensPerDollar = 26 * 1 ether;
  uint256 public publicsaleRoundOneTokensPerDollar = 24 * 1 ether;
  uint256 public publicsaleRoundTwoTokensPerDollar = 20 * 1 ether;
  enum Stages {CrowdSaleNotStarted,Pause, Private, PrivateEnd, PreIcoOne, PreIcoOneEnd, PreIcoTwo, PreIcoTwoEnd, PublicIcoOne, PublicIcoOneEnd, PublicIcoTwo, PublicIcoTwoEnd}
  Stages currentStage;
  Stages previousStage;
  bool public Paused;

  // adreess vs state mapping (1 for exists , zero default);
  mapping (address => bool) public whitelistedContributors;
  
  modifier CrowdsaleStarted(){
      require(crowdsaleStarted);
      _;
  }
 
  /**
   * Event for token purchase logging
   * @param purchaser who paid for the tokens
   * @param beneficiary who got the tokens
   * @param value weis paid for purchase
   * @param amount amount of tokens purchased
   */
  event TokenPurchase(address indexed purchaser, address indexed beneficiary, uint256 value, uint256 amount);

  /**
   *@dev initializes the crowdsale contract 
   * @param _newOwner Address who has special power to change the ether price in cents according to the market price
   * @param _wallet Address where collected funds will be forwarded to
   * @param _token Address of the token being sold
   *  @param _ethPriceInCents ether price in cents
   */
    constructor(address _newOwner, address _wallet, DiscoveryToken _token,uint256 _ethPriceInCents) Owned(_newOwner) public {
        require(_wallet != address(0));
        require(_token != address(0));
        require(_ethPriceInCents > 0);
        wallet = _wallet;
        owner = _newOwner;
        token = _token;
        ethPrice = _ethPriceInCents; //ethPrice in cents
        currentStage = Stages.CrowdSaleNotStarted;
        oraclize_setProof(proofType_TLSNotary | proofStorage_IPFS);
        LogConstructorInitiated("Constructor was initiated. Call 'update()' to send the Oraclize Query.");
    }

    
    // Begin : oraclize related functions 
    function __callback(bytes32 myid, string result, bytes proof) public {
        if (msg.sender != oraclize_cbAddress()) revert();
        ethPrice = parseInt(result, 2);
        newPriceTicker(myid, result, proof); //event
        if (updateTime > 0) updateAfter(updateTime);
    }

    function update() public onlyOwner {
        if (updateTime > 0) updateTime = 0;
        if (oraclize_getPrice("URL", CUSTOM_GASLIMIT) > this.balance) {
            newOraclizeQuery("Oraclize query was NOT sent, please add some ETH to cover for the query fee"); //event
        } else {
            newOraclizeQuery("Oraclize query was sent, standing by for the answer.."); //event
            oraclize_query("URL", "json(https://api.kraken.com/0/public/Ticker?pair=ETHUSD).result.XETHZUSD.c.0", CUSTOM_GASLIMIT);
        }
    }

    function updatePeriodically(uint256 _updateTime) public onlyOwner {
        updateTime = _updateTime;
        if (oraclize_getPrice("URL", CUSTOM_GASLIMIT) > this.balance) {
            newOraclizeQuery("Oraclize query was NOT sent, please add some ETH to cover for the query fee");
        } else {
            newOraclizeQuery("Oraclize query was sent, standing by for the answer..");
            oraclize_query("URL", "json(https://api.kraken.com/0/public/Ticker?pair=ETHUSD).result.XETHZUSD.c.0", CUSTOM_GASLIMIT);
        }
    }

    function updateAfter(uint256 _updateTime) internal {
        if (oraclize_getPrice("URL", CUSTOM_GASLIMIT) > this.balance) {
            newOraclizeQuery("Oraclize query was NOT sent, please add some ETH to cover for the query fee");
        } else {
            newOraclizeQuery("Oraclize query was sent, standing by for the answer..");
            oraclize_query(_updateTime, "URL", "json(https://api.kraken.com/0/public/Ticker?pair=ETHUSD).result.XETHZUSD.c.0", CUSTOM_GASLIMIT);
        }
    }

    // END : oraclize related functions 

   /**
   * @dev calling this function will start the sale
   */
  function pause() public onlyOwner
    {
      require(crowdsaleStarted == true);
      require(Paused == false);
      previousStage=currentStage;
      currentStage=Stages.Pause;
      Paused = true;
    }
  
  function restartSale() public onlyOwner
    {
     require(currentStage==Stages.Pause);
     currentStage=previousStage;
     Paused = false;
    }   

   
  function endPrivate() public onlyOwner {
    require(currentStage == Stages.Private);
    currentStage = Stages.PrivateEnd;
  }

  function startPreIcoOne() public onlyOwner {
    require(currentStage == Stages.PrivateEnd);
    currentStage = Stages.PreIcoOne;
   
  }

  function endPreIcoOne() public onlyOwner {
    require(currentStage == Stages.PreIcoOne);
    currentStage = Stages.PreIcoOneEnd;
  }

  function startPreIcoTwo() public onlyOwner {
    require(currentStage == Stages.PreIcoOneEnd);
    currentStage = Stages.PreIcoTwo;
   
  }

  function endPreIcoTwo() public onlyOwner {
    require(currentStage == Stages.PreIcoTwo);
    currentStage = Stages.PreIcoTwoEnd;
  }

  function startIcoOne() public onlyOwner {
    require(currentStage == Stages.PreIcoTwoEnd);
    currentStage = Stages.PublicIcoOne;
  }

  function endIcoOne() public onlyOwner {
    require(currentStage == Stages.PublicIcoOne);
    currentStage = Stages.PublicIcoOneEnd;
    
  }


  function startIcoTwo() public onlyOwner {
    require(currentStage == Stages.PublicIcoOneEnd);
    currentStage = Stages.PublicIcoTwo;
  }

  function endIcoTwo() public onlyOwner {
    require(currentStage == Stages.PublicIcoTwo);
    currentStage = Stages.PublicIcoTwoEnd;
  }

  function getStage() public view returns (string) {
    if (currentStage == Stages.Private) return 'Private sale';
    else if (currentStage == Stages.PrivateEnd) return 'Private sale end';
    else if (currentStage == Stages.PreIcoOne) return 'Pre ICO Round One';
    else if (currentStage == Stages.PreIcoOneEnd) return 'Pre ICO Round One End';
    else if (currentStage == Stages.PreIcoTwo) return 'Pre ICO Round Two';
    else if (currentStage == Stages.PreIcoTwoEnd) return 'Pre ICO Round Two End';
    else if (currentStage == Stages.PublicIcoOne) return 'Public Ico Round One';
    else if (currentStage == Stages.PublicIcoOneEnd) return 'Public Ico Round One End';
    else if (currentStage == Stages.PublicIcoTwo) return 'Public Ico Round Two';
    else if (currentStage == Stages.PublicIcoTwoEnd) return 'Public Ico Round Two End';
    else if (currentStage == Stages.Pause) return 'Paused';
    else if (currentStage == Stages.CrowdSaleNotStarted) return 'CrowdSale  Not Started yet';
   }
    function startCrowdsale() onlyOwner public {
        require(!crowdsaleStarted);
        crowdsaleStarted = true;
        currentStage = Stages.Private;
      
    }
  
  // -----------------------------------------
  // Crowdsale external interface
  // -----------------------------------------

  /**
   * @dev fallback function ***DO NOT OVERRIDE***
   */
  function () external payable {
    if(msg.sender != owner)
    {
      buyTokens(msg.sender);
    }
    else
    {
      revert();
    }
  }


  /**
   * @param _beneficiary Address performing the token purchase
   */
  function buyTokens(address _beneficiary) CrowdsaleStarted public payable {
    require(whitelistedContributors[_beneficiary] == true );
    require(Paused != true);
    uint256 weiAmount = msg.value;
    require(weiAmount > 0);
    require(ethPrice > 0);
    uint256 usdCents = weiAmount.mul(ethPrice).div(1 ether); 

    // calculate token amount to be created
    uint256 tokens = _getTokenAmount(usdCents);

    _validateCapLimits(usdCents,msg.value);

    // update state
    totalRaisedInCents = totalRaisedInCents.add(usdCents);
    _processPurchase(_beneficiary,tokens);
     emit TokenPurchase(msg.sender, _beneficiary, weiAmount, tokens);
     wallet.transfer(msg.value);
  }
  
 
  /**
   * @dev sets the value of ether price in cents.Can be called only by the owner account.
   * @param _ethPriceInCents price in cents .
   */
    function setEthPriceInCents(uint _ethPriceInCents) onlyOwner public returns(bool) {
        ethPrice = _ethPriceInCents;
        return true;
    }

  // -----------------------------------------
  // Internal interface (extensible)
  // -----------------------------------------


  /**
   * @dev Validation of the capped restrictions.
   * @param _cents cents amount
   */
  function _validateCapLimits(uint256 _cents,uint256 _wei) internal {
     require(_wei >= 1 ether);
     
    if (currentStage == Stages.Private) {
       privateSaleRaised = privateSaleRaised.add(_cents);
       require(privateSaleRaised <= privateSaleCap);
      
    } else if(currentStage == Stages.PreIcoOne) {
      presaleRoundOneRaised = presaleRoundOneRaised.add(_cents);
      require(presaleRoundOneRaised <= presaleRoundOneCap);
    
    } else if(currentStage == Stages.PreIcoTwo) {
      presaleRoundTwoRaised = presaleRoundTwoRaised.add(_cents);
      require(presaleRoundTwoRaised <= presaleRoundTwoCap);
    
    } else if(currentStage == Stages.PublicIcoOne) {
      publicsaleRoundOneRaised = publicsaleRoundOneRaised.add(_cents);
      require(publicsaleRoundOneRaised <= publicsaleRoundOneCap);
    }
     else if(currentStage == Stages.PublicIcoTwo) {
      publicsaleRoundTwoRaised = publicsaleRoundTwoRaised.add(_cents);
      require(publicsaleRoundTwoRaised < publicsaleRoundTwoCap);
    }
     else {
      revert();
    }
  }

  /**
   * @dev Source of tokens. Override this method to modify the way in which the crowdsale ultimately gets and sends its tokens.
   * @param _beneficiary Address performing the token purchase
   * @param _tokenAmount Number of tokens to be emitted
   */
  function _deliverTokens(address _beneficiary, uint256 _tokenAmount) internal {
    require(token.saleTransfer(_beneficiary, _tokenAmount));
  }

  /**
   * @dev Executed when a purchase has been validated and is ready to be executed. Not necessarily emits/sends tokens.
   * @param _beneficiary Address receiving the tokens
   * @param _tokenAmount Number of tokens to be purchased
   */
  function _processPurchase(address _beneficiary, uint256 _tokenAmount) internal {
    _deliverTokens(_beneficiary, _tokenAmount);
  }
  

  /**
   * @param _usdCents Value in usd cents to be converted into tokens
   * @return Number of tokens that can be purchased with the specified _usdCents
   */
  function _getTokenAmount(uint256 _usdCents) CrowdsaleStarted public view returns (uint256) {
    uint256 tokens;
    

    if (currentStage == Stages.Private)  tokens = _usdCents.div(100).mul(privateSaleTokensPerDollar);
    if (currentStage == Stages.PreIcoOne) tokens = _usdCents.div(100).mul(presaleRoundOneTokensPerDollar);
    if (currentStage == Stages.PreIcoTwo) tokens = _usdCents.div(100).mul(presaleRoundTwoTokensPerDollar);
    if (currentStage == Stages.PublicIcoOne)  tokens = _usdCents.div(100).mul(publicsaleRoundOneTokensPerDollar);
    if (currentStage == Stages.PublicIcoTwo)  tokens = _usdCents.div(100).mul(publicsaleRoundTwoTokensPerDollar);
    

    return tokens;
  }
  

    /**
       * @dev burn the unsold tokens.
       
       */
    function burnTokens() public onlyOwner {
        require(currentStage == Stages.PublicIcoTwoEnd);
        require(token.burnTokensForSale());
    }
        
  /**
   * @dev finalize the crowdsale.After finalizing ,tokens transfer can be done.
   */
    function finalizeSale() public  onlyOwner {
        require(currentStage == Stages.PublicIcoTwoEnd);
        token.finalize();
    }
  
  

   /**
   * @dev whitelist addresses of investors.
   * @param addrs ,array of addresses of investors to be whitelisted
   * Note:= Array length must be less than 200.
   */
    function authorizeKyc(address[] addrs) external onlyOwner returns (bool success) {
        uint arrayLength = addrs.length;
        for (uint x = 0; x < arrayLength; x++) {
            whitelistedContributors[addrs[x]] = true;
        }

        return true;
    }

}