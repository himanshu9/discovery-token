
pragma solidity 0.4.24;

import "./Owned.sol";
import "./SafeMath.sol";


contract DiscoveryToken {
    function vestingTransfer(address, uint256) public returns (bool) {}
    function balanceOf(address) public view returns (uint256);
}


/**
 * @title TokenVesting
 * @dev A token holder contract that can release its token balance gradually like a
 * typical vesting scheme, with a cliff and vesting period. Optionally revocable by the
 * owner.
 */
contract TokenVesting is Owned {
    using SafeMath for uint256;
    event Released(uint256 amount);
    event Revoked(address);
    DiscoveryToken public _token;
    uint256 public teamTokensSent;
    uint256 public advisorsTokensSent;
    uint256 public marketingTokensSent;
    uint256 public teamVestableTokens = 120000000 * 1 ether;
    uint256 public advisorsVestableTokens = 70000000 * 1 ether;
    uint256 public marketingVestableTokens = 25000000 * 1 ether;
    mapping (address => uint256) public released;
    mapping (address => bool) public revoked;
    struct beneficiary {
        address beneficiaryAddress;
        uint256 startTime;
        uint256 duration;
        bool revocable;
        uint256 tokens;
        uint member;
    }
    mapping(address => beneficiary) public beneficiaries;
    constructor(DiscoveryToken token,address _owner) Owned(_owner) public {
      _token = token;
    }
    
  /**
    * @dev Creates a vesting contract that vests its balance of any ERC20 token to the
    * _beneficiary, gradually in a linear fashion until _start + _duration. By then all
    * of the balance will have vested.
    * @param _beneficiary address of the beneficiary to whom vested tokens are transferred
    * @param _cliff the time duration in addition to current time after which  which vesting starts
    * @param _duration duration in seconds of the period in which the tokens will vest
    * @param _revocable whether the vesting is revocable or not
    * @param _member ,uint value to identify vesting beneficiary ( 1 for core team,2 for advisor and 3 for marketing )
  */

  function vestTokens(address _beneficiary,uint256 _cliff, uint256 _duration, uint256 _tokens, 
  bool _revocable, uint256 _member) public onlyOwner {    
      require(_beneficiary != address(0));
      require(beneficiaries[_beneficiary].beneficiaryAddress == (0x0));
      uint256 tokens = _tokens * 1 ether;
      beneficiaries[_beneficiary].beneficiaryAddress  = _beneficiary;
      beneficiaries[_beneficiary].startTime = now.add(_cliff);
      beneficiaries[_beneficiary].duration  = _duration;
      beneficiaries[_beneficiary].revocable = _revocable;
      beneficiaries[_beneficiary].tokens = _tokens * 1 ether;
      beneficiaries[_beneficiary].member = _member;
      if(_member == 1 ) {
        teamTokensSent = teamTokensSent.add(tokens);
        require(teamTokensSent <= teamVestableTokens); 
      } else if(_member == 2) {
        advisorsTokensSent = advisorsTokensSent.add(tokens);
        require(advisorsTokensSent <= advisorsVestableTokens);
      } else if(_member == 3) {
        marketingTokensSent = marketingVestableTokens.add(tokens);
        require(marketingTokensSent <= marketingVestableTokens);
      } else {
          revert();
      }
  }

  /**
    * @notice Transfers vested tokens to beneficiary.
    * @param  _beneficiary address of benefeciary for which tokens are getting vested
  */
  function release(address _beneficiary) public {
    uint256 unreleased = releasableAmount(_beneficiary);
    require(unreleased > 0);
    released[_beneficiary] = released[_beneficiary].add(unreleased);
    _token.vestingTransfer(_beneficiary, unreleased);
    emit Released(unreleased);
  }



/**
  * @notice Allows the owner to revoke the vesting. Tokens already vested
  * will be sent to benefeciary, the rest are returned back to contract for future vesting.
  * @param _beneficiary address of benefeciary for which tokens are getting vested
*/
function revoke(address _beneficiary) public onlyOwner {
  require(beneficiaries[_beneficiary].revocable);
  require(!revoked[_beneficiary]);
  uint256 unreleased = releasableAmount(_beneficiary);
  uint256 refund = beneficiaries[_beneficiary].tokens - unreleased;
  released[_beneficiary] = beneficiaries[_beneficiary].tokens;
  beneficiaries[_beneficiary].tokens = unreleased;
  _token.vestingTransfer(_beneficiary, unreleased);
  _token.vestingTransfer(this, refund);
  revoked[_beneficiary] = true;
  
  if( beneficiaries[_beneficiary].member == 1 ) {
      teamTokensSent = teamTokensSent.sub(refund);
     
  }
  else if( beneficiaries[_beneficiary].member == 2){
      advisorsTokensSent = advisorsTokensSent.sub(refund);
      
  }
  else if(  beneficiaries[_beneficiary].member == 3) {
      marketingTokensSent = marketingTokensSent.sub(refund);
     
  }
  
  emit Revoked(_beneficiary);
}

  /**
   * @dev Calculates the amount that has already vested but hasn't been released yet.
   * @param _beneficiary address of benefeciary for which tokens are getting vested
   */
  function releasableAmount(address _beneficiary) public view returns (uint256) {
    return vestedAmount(_beneficiary).sub(released[_beneficiary]);
  }

  /**
   * @dev Calculates the amount that has already vested.
   * @param _beneficiary address of benefeciary for which tokens are getting vested
   */
  function vestedAmount(address _beneficiary) public view returns (uint256) {
    uint256 totalBalance = beneficiaries[_beneficiary].tokens;
    if (block.timestamp >= beneficiaries[_beneficiary].startTime.add(beneficiaries[_beneficiary].duration) || revoked[_beneficiary]) {
      return totalBalance;
    } else {
      return totalBalance.mul(block.timestamp.sub( beneficiaries[_beneficiary].startTime)).div( beneficiaries[_beneficiary].duration);
    }
  }
}