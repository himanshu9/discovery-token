const DiscoveryToken = artifacts.require('DiscoveryToken.sol');
const TokenVesting = artifacts.require('TokenVesting.sol');
const Crowdsale = artifacts.require('Crowdsale.sol');
//const { expectThrow } = require('openzeppelin-solidity/test/helpers/expectThrow');
//const { EVMRevert } = require('openzeppelin-solidity/test/helpers/EVMRevert');
var Web3 = require("web3");
var web3 = new Web3(new Web3.providers.HttpProvider("http://localhost:8545"));

contract('DiscoveryToken', async (accounts) => {

  it('Should correctly initialize constructor values of Token Contract', async () => {

    this.tokenhold = await DiscoveryToken.new(accounts[0]);
    let totalSupply = await this.tokenhold.totalSupply.call();
    let owner = await this.tokenhold.owner.call();
    assert.equal(totalSupply.toNumber(), 1000000000000000000000000000);
    assert.equal(owner, accounts[0]);

  });

  it("Should Deploy Crowdsale only", async () => {

    this.crowdhold = await Crowdsale.new(accounts[0], accounts[9], this.tokenhold.address, 10000, { gas: 600000000 });

  });

  it("Should Deploy Vesting  only", async () => {
    var PauseCondition = await this.tokenhold.paused.call();
    assert.equal(PauseCondition, false, 'Not Paused');
    this.vesthold = await TokenVesting.new(this.tokenhold.address, accounts[0], { gas: 500000000 });
  });

  it("Should Activate Sale contract", async () => {

    var PauseCondition = await this.tokenhold.paused.call();
    assert.equal(PauseCondition, false, 'Not Paused');
    var Activate = await this.tokenhold.activateSaleContract(this.crowdhold.address, { gas: 500000000 });

  });

  it("Should check balance of Crowdsale after, crowdsale activate from token contract", async () => {
    let balancOfCrowdsale = 510000000000000000000000000;
    var balance = await this.tokenhold.balanceOf.call(this.crowdhold.address, { gas: 500000000 });
    assert.equal(balance.toNumber(), balancOfCrowdsale);
  });

  it("Should check balance of Vesting Contract, after activate vesting Contract", async () => {
    await this.tokenhold.activateVestingContract(this.vesthold.address, { gas: 500000000 });
    let balancOfVest = 215000000000000000000000000;
    var balanceVest = await this.tokenhold.balanceOf.call(this.vesthold.address, { gas: 500000000 });
    assert.equal(balanceVest.toNumber(), balancOfVest);
  });

  it("Should Authorize KYC first", async () => {

    var whiteListAddress = await this.crowdhold.whitelistedContributors.call(accounts[1], { gas: 500000000 });
    assert.equal(whiteListAddress, false, 'not white listed');
    var authorizeKYC = await this.crowdhold.authorizeKyc([accounts[1]], { from: accounts[0] });
    var whiteListAddressNow = await this.crowdhold.whitelistedContributors.call(accounts[1]);
    assert.equal(whiteListAddressNow, true, ' now white listed');

  });

  it("Should Freeze Account", async () => {

    var frozen_Account = await this.tokenhold.frozenAccounts.call(accounts[4], { gas: 500000000 });
    assert.equal(frozen_Account, false, 'not freeze Account');
    var freezeAccounts = await this.tokenhold.freezeAccount([accounts[4]], 'true', { from: accounts[0] });
    var frozen_Account = await this.tokenhold.frozenAccounts.call(accounts[4], { gas: 500000000 });
    assert.equal(frozen_Account, true, ' Account Freezed');
  });

  it("Should Start CrowdSale ", async () => {

    var getTheStagebefore12 = await this.crowdhold.Paused.call();
    assert.equal(getTheStagebefore12, false, 'Not Paused');
    var crowdsaleStart = await this.crowdhold.startCrowdsale({ from: accounts[0] });
    var getTheStage = await this.crowdhold.getStage.call();
    var _presale = 'Private sale';
    assert.equal(getTheStage, _presale, 'Sale started now');

  });


  it("Should revert if owner tries to transfer zero bounty  ", async () => {

    try {
      var sendbountyTokens1 = await this.tokenhold.sendBounty(accounts[3], 0, { gas: 5000000 });
      var BountySendValue1 = 0;
      var balanceOfbounty1 = await this.tokenhold.balanceOf.call(accounts[3]);
      assert.equal(balanceOfbounty1.toNumber(), BountySendValue1, 'Sale started now');

    }
    catch (error) {
      var error_ = 'VM Exception while processing transaction: revert';
      assert.equal(error.message, error_, 'Token ammount');

    }
  });

  it("Should revert if owner tries to transfer zero Partner Tokens", async () => {

    try {
      var balanceOfPartnerbefore1 = await this.tokenhold.balanceOf.call(accounts[4]);
      var partnerTokens1 = await this.tokenhold.releasePartnersTokens(accounts[4], 0, { gas: 5000000 });
      var partnerSendValue1 = 0;
      var balanceOfPartner1 = await this.tokenhold.balanceOf.call(accounts[4]);
      assert.equal(balanceOfPartner1.toNumber(), partnerSendValue1, 'Balance of partner after sending partner token');
    } catch (error) {
      var error_ = 'VM Exception while processing transaction: revert';
      assert.equal(error.message, error_, 'Token ammount');
    }
  });

  it("Should revert if user tries to transfer zero Treasury  Tokens  ", async () => {

    try {
      var balanceOfPartnerbefore2 = await this.tokenhold.balanceOf.call(accounts[5]);
      var partnerTokens2 = await this.tokenhold.releaseTreasuryTokens(accounts[5], 0, { gas: 5000000 });
      var partnerSendValue2 = 0;
      var balanceOfPartner2 = await this.tokenhold.balanceOf.call(accounts[5]);
      assert.equal(balanceOfPartner2.toNumber(), partnerSendValue2, 'Balance of Treasury after sending partner token');
    } catch (error) {
      var error_ = 'VM Exception while processing transaction: revert';
      assert.equal(error.message, error_, 'Token ammount');
    }
  });


  it("Should send Bounty Tokens  ", async () => {

    var PauseConditionB = await this.tokenhold.paused.call();
    assert.equal(PauseConditionB, false, 'Not Paused');
    let initialBounty = await this.tokenhold.bountyTokens.call();
    var sendbountyTokens = await this.tokenhold.sendBounty(accounts[3], 5, { gas: 5000000 });
    var BountySendValue = 5;
    var balanceOfbounty = await this.tokenhold.balanceOf.call(accounts[3]);
    let bountyLeft = await this.tokenhold.bountyTokens.call();
    assert.equal(balanceOfbounty.toNumber() / 10 ** 18, BountySendValue, 'Wrong bounty sent');
  });

  it("Should Released Partners Tokens  ", async () => {

    var PauseConditionP = await this.tokenhold.paused.call();
    assert.equal(PauseConditionP, false, 'Not Paused');
    var balanceOfPartnerbefore = await this.tokenhold.balanceOf.call(accounts[4]);
    var partnerTokens = await this.tokenhold.releasePartnersTokens(accounts[4], 5, { gas: 5000000 });
    var partnerSendValue = 5000000000000000000;
    var balanceOfPartner = await this.tokenhold.balanceOf.call(accounts[4]);
    assert.equal(balanceOfPartner.toNumber(), partnerSendValue, 'Balance of partner after sending partner token');

  });

  it("Should release Treasury  Tokens  ", async () => {

    var PauseConditionP = await this.tokenhold.paused.call();
    assert.equal(PauseConditionP, false, 'Not Paused');
    var balanceOfPartnerbefore = await this.tokenhold.balanceOf.call(accounts[5]);
    var partnerTokens = await this.tokenhold.releaseTreasuryTokens(accounts[5], 5, { gas: 5000000 });
    var partnerSendValue = 5000000000000000000;
    var balanceOfPartner = await this.tokenhold.balanceOf.call(accounts[5]);
    assert.equal(balanceOfPartner.toNumber(), partnerSendValue, 'Balance of Treasury after sending partner token');

  });

  it("Should vest Token of vesting contract when tokens are not revokable ", async () => {

    var vestTokens = await this.vesthold.vestTokens(accounts[6], 0, 0, 5, 'false', 1);
    var vestAmount = await this.vesthold.vestedAmount(accounts[6]);
    var ammount_1 = 5000000000000000000;
    assert.equal(vestAmount.toNumber(), ammount_1, 'vestable balance');

  });

  it("should Approve address to spend specific token ", async () => {

    var PauseConditionP = await this.tokenhold.paused.call();
    assert.equal(PauseConditionP, false, 'Not Paused');
    this.tokenhold.approve(accounts[3], 200000);
    let allowance = await this.tokenhold.allowance.call(accounts[0], accounts[3]);
    assert.equal(allowance, 200000, "allowance is wrong");
  });

  it("Should be able to buy Tokens  according to private sale", async () => {

    var getTheStagebefore12 = await this.crowdhold.Paused.call();
    assert.equal(getTheStagebefore12, false, 'Not Paused');
    let fundWalletBefore = await web3.eth.getBalance(accounts[9]);
    let AccountBalance_oneBefore = await web3.eth.getBalance(accounts[1]);
    var buy_Tokens = await this.crowdhold.buyTokens(accounts[1], { from: accounts[1], value: web3.utils.toWei("1", "ether") });
    var tokens = 4000;
    var balance_after = await this.tokenhold.balanceOf.call(accounts[1]);
    let fundWalletAfter = await web3.eth.getBalance(accounts[9]);
    let AccountBalance_oneAfter = await web3.eth.getBalance(accounts[1]);
    assert.equal(balance_after.toNumber(), tokens * (10 ** 18), 'Token ammount');
  });

  it("Should Not be able to participate when it is paused", async () => {

    try {
      var getTheStagebefore1 = await this.crowdhold.getStage.call();
      var stageBefore1 = 'Private sale';
      assert.equal(getTheStagebefore1, stageBefore1, 'Private sale');
      await this.crowdhold.pause({ from: accounts[0] });
      let stageNow = await this.crowdhold.getStage();
      assert.equal(stageNow, 'Paused');
      var buy_Tokens = await this.crowdhold.buyTokens(accounts[1], { from: accounts[1], value: web3.utils.toWei("1", "ether") });
    } catch (error) {
      var error_ = 'VM Exception while processing transaction: revert';
      assert.equal(error.message, error_, 'Token ammount');
    }
  });

  it("Should be able to Un pause The sale", async () => {

    let stageNow1 = await this.crowdhold.getStage();
    assert.equal(stageNow1, 'Paused');
    await this.crowdhold.restartSale();
    let stage2 = await this.crowdhold.getStage();
    assert.equal(stage2, "Private sale", "Stage is wrong");
  });

  it("Should be able to end private sale", async () => {

    var getTheStagebefore12 = await this.crowdhold.Paused.call();
    assert.equal(getTheStagebefore12, false, 'Not Paused');
    await this.crowdhold.endPrivate();
    let stage = await this.crowdhold.getStage();
    assert.equal(stage, "Private sale end", "Stage is wrong");
  });

  it("Should be able to start pre sale round one and buy tokens according to that", async () => {

    var getTheStagebefore12 = await this.crowdhold.Paused.call();
    assert.equal(getTheStagebefore12, false, 'Not Paused');
    await this.crowdhold.startPreIcoOne();
    let stage = await this.crowdhold.getStage();
    assert.equal(stage, "Pre ICO Round One", "Stage is wrong");
    var balance_Before = await this.tokenhold.balanceOf.call(accounts[1]);
    var buy_Tokens = await this.crowdhold.buyTokens(accounts[1], { from: accounts[1], value: web3.utils.toWei("1", "ether") });
    var tokens = 2800;
    var balance_after = await this.tokenhold.balanceOf.call(accounts[1]);
    let fundWalletAfter = await web3.eth.getBalance(accounts[9]);
    let AccountBalance_oneAfter = await web3.eth.getBalance(accounts[1]);
    assert.equal(balance_after.toNumber(), balance_Before.toNumber() + (tokens * (10 ** 18)), 'Token ammount');
  });

  it("Should be able to pause and unPause Crowdsale contract", async () => {

    var getTheStagebefore12 = await this.crowdhold.Paused.call();
    assert.equal(getTheStagebefore12, false, 'Not Paused');
    var getTheStagebefore1 = await this.crowdhold.getStage.call();
    var stageBefore1 = 'Pre ICO Round One';
    assert.equal(getTheStagebefore1, stageBefore1, 'Pre ICO Round One');
    var pauseStautsBefore = await this.crowdhold.Paused.call();
    assert.equal(pauseStautsBefore, false, 'Unpaused');
    var pause = await this.crowdhold.pause();
    var pauseStatusAfter = await this.crowdhold.Paused.call();
    assert.equal(pauseStatusAfter, true, 'Now Paused');
    var restartSale = await this.crowdhold.restartSale();
    var pauseStatusAfter1 = await this.crowdhold.Paused.call();
    assert.equal(pauseStatusAfter1, false, 'again UnPaused');
  });

  it("Should be able to end pre sale round one,start pre sale round two and buy tokens according to that", async () => {
    var getTheStagebefore12 = await this.crowdhold.Paused.call();
    assert.equal(getTheStagebefore12, false, 'Not Paused');
    await this.crowdhold.endPreIcoOne();
    let stage = await this.crowdhold.getStage();
    assert.equal(stage, "Pre ICO Round One End", "Stage is wrong");
    await this.crowdhold.startPreIcoTwo();
    let newStage = await this.crowdhold.getStage();
    assert.equal(newStage, "Pre ICO Round Two", "Stage is wrong");
    var balance_Before = await this.tokenhold.balanceOf.call(accounts[1]);
    var buy_Tokens = await this.crowdhold.buyTokens(accounts[1], { from: accounts[1], value: web3.utils.toWei("1", "ether") });
    var tokens = 2600;
    var balance_after1 = await this.tokenhold.balanceOf.call(accounts[1]);
    let fundWalletAfter = await web3.eth.getBalance(accounts[9]);
    let AccountBalance_oneAfter = await web3.eth.getBalance(accounts[1]);
    assert.equal(balance_after1.toNumber(), balance_Before.toNumber() + (tokens * (10 ** 18)), 'Token ammount');
  });

  it("Should be able to end pre sale round two,start public sale round one and buy tokens according to that", async () => {
    var getTheStagebefore12 = await this.crowdhold.Paused.call();
    assert.equal(getTheStagebefore12, false, 'Not Paused');
    await this.crowdhold.endPreIcoTwo();
    let stage = await this.crowdhold.getStage();
    assert.equal(stage, "Pre ICO Round Two End", "Stage is wrong");
    await this.crowdhold.startIcoOne();
    let newStage = await this.crowdhold.getStage();
    assert.equal(newStage, "Public Ico Round One", "Stage is wrong");
    var balance_Before = await this.tokenhold.balanceOf.call(accounts[1]);
    var buy_Tokens = await this.crowdhold.buyTokens(accounts[1], { from: accounts[1], value: web3.utils.toWei("1", "ether") });
    var tokens = 2400;
    var balance_after2 = await this.tokenhold.balanceOf.call(accounts[1]);
    let fundWalletAfter = await web3.eth.getBalance(accounts[9]);
    let AccountBalance_oneAfter = await web3.eth.getBalance(accounts[1]);
    assert.equal(balance_after2.toNumber(), balance_Before.toNumber() + (tokens * (10 ** 18)), 'Token ammount');


  });

  it("Should be able to end public sale round one,start public sale round two and buy tokens according to that", async () => {
    var getTheStagebefore12 = await this.crowdhold.Paused.call();
    assert.equal(getTheStagebefore12, false, 'Not Paused');
    await this.crowdhold.endIcoOne();
    let stage = await this.crowdhold.getStage();
    assert.equal(stage, "Public Ico Round One End", "Stage is wrong");
    await this.crowdhold.startIcoTwo();
    let newStage = await this.crowdhold.getStage();
    assert.equal(newStage, "Public Ico Round Two", "Stage is wrong");
    var balance_Before = await this.tokenhold.balanceOf.call(accounts[1]);
    var buy_Tokens = await this.crowdhold.buyTokens(accounts[1], { from: accounts[1], value: web3.utils.toWei("1", "ether") });
    var tokens = 2000;
    var balance_after3 = await this.tokenhold.balanceOf.call(accounts[1]);
    let fundWalletAfter = await web3.eth.getBalance(accounts[9]);
    let AccountBalance_oneAfter = await web3.eth.getBalance(accounts[1]);
    assert.equal(balance_after3.toNumber(), balance_Before.toNumber() + (tokens * (10 ** 18)), 'Token ammount');
  });


  it("Should be able to end public sale round two and burn tokens", async () => {
    var getTheStagebefore12 = await this.crowdhold.Paused.call();
    assert.equal(getTheStagebefore12, false, 'Not Paused');
    await this.crowdhold.endIcoTwo();
    let stage = await this.crowdhold.getStage();
    assert.equal(stage, "Public Ico Round Two End", "Stage is wrong");
    var contract_before = await this.tokenhold.balanceOf.call(this.crowdhold.address);
    await this.crowdhold.burnTokens();
    var contract_after = await this.tokenhold.balanceOf.call(this.crowdhold.address);
    assert.equal(contract_after.toNumber(), 0, "not able to correctly burn tokens");
  });


  it("Should be not be able to transfer tokens before sale is finalised", async () => {

    try {
      await this.tokenhold.transfer(accounts[5], 100, { from: accounts[1] });
    } catch (error) {
      var error_ = 'VM Exception while processing transaction: revert';
      assert.equal(error.message, error_, 'Token ammount');
    }

  });

  it("Should be  able to transfer tokens after sale is finalised", async () => {
    
    let fundBefore = await this.tokenhold.fundraising.call();
    await this.crowdhold.finalizeSale();
    let fund = await this.tokenhold.fundraising.call();
    await this.tokenhold.transfer(accounts[5], 100, { from: accounts[1] });
    let balanceOfFive = this.tokenhold.balanceOf(accounts[5]);
  });

  it("Should vest Token of vesting contract when tokens are revocable ", async () => {

    var vestTokens1 = await this.vesthold.vestTokens(accounts[4], 0, 0, 3, 'true', 1);
    var vestAmount1 = await this.vesthold.vestedAmount(accounts[4]);
    var ammount_2 = 3000000000000000000;
    assert.equal(vestAmount1.toNumber(), ammount_2, 'vestable balance');
  });

  it("Should be able to transfer ownership of Vesting Contract ", async () => {

    let ownerOld1 = await this.vesthold.owner.call();
    let newowner1 = await this.vesthold.transferOwnership(accounts[4], { from: accounts[0] });
    let acceptOwner = await this.vesthold.acceptOwnership({ from: accounts[4] });
    let ownerNew1 = await this.vesthold.owner.call();
    assert.equal(ownerNew1, accounts[4], 'Transfered ownership');

  });

  it("Should not be able to transfer zero tokens  ", async () => {

    try {
      var beforetrans = await this.tokenhold.balanceOf.call(accounts[6]);
      var balanceOfBeneficiary3 = await this.tokenhold.balanceOf.call(accounts[1]);
      var trans = await this.tokenhold.transfer(accounts[6], 0);
      var aftertrans = await this.tokenhold.balanceOf.call(accounts[6]);
    } catch (error) {
      var error_ = 'VM Exception while processing transaction: revert';
      assert.equal(error.message, error_, 'Token ammount');
    }

  });

  it("Should be able to set ether price ", async () => {

    var currentEthprice = 10000;
    var toBeEthprice = 500000;
    var ethpricebefore = await this.crowdhold.ethPrice.call();
    assert.equal(ethpricebefore.toNumber(), currentEthprice, 'ether price before');
    var setEthPrice = await this.crowdhold.setEthPriceInCents(500000, { from: accounts[0] });
    var ethpricenow = await this.crowdhold.ethPrice.call();
    assert.equal(ethpricenow.toNumber(), toBeEthprice, 'ether price After');

  });

  it("should not increase Approval for Negative Tokens", async () => {

    try {

      this.tokenhold.changeApproval(accounts[9], 2000000000000000000, -1000000000000000000, { from: accounts[4] });

    }
    catch (error) {
      var error_ = 'VM Exception while processing transaction: invalid opcode';
      assert.equal(error.message, error_, 'Token ammount');

    }
  });

  it("should Not Approve address to spend Negative token ", async () => {

    try {
      this.tokenhold.approve(accounts[9], -1000000000000000000, { from: accounts[4] });
    } catch (error) {
      var error_ = 'VM Exception while processing transaction: invalid opcode';
      assert.equal(error.message, error_, 'Token ammount');
    }

  });

})

